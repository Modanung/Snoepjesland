void setup()
{
  size(1000,600);
}

void draw()
{
//Teken lollies  
PShape lolly = loadShape("lolly snoepjesland .svg");
lolly.scale(0.3);
PShape lolly2 = loadShape("Lolly.svg");
lolly2.scale(0.3);

//Achtergrondkleur
background(245,150,247);

//Teken wolk
stroke(255,0,255);
fill(255,0,255);
ellipse(70,70,50,50);
ellipse(115,54,50,50);
ellipse(116,90,50,50);
ellipse(92,110,50,50);
ellipse(51,108,50,50);
ellipse(33,65,50,50);
ellipse(47,32,50,50);
ellipse(80,28,50,50);

//Teken gras
fill(0,255,0);
stroke(0,255,0);
rect(0,580,1000,600);
fill(252,87,106);
shape(lolly,-150,280);
shape(lolly2,800,320);
strokeWeight(3);

//Probeer Ctrl+T eens (Edit->Auto Format)

//Teken hagelslag
for(int h=0; h <1000;++h)
{int kleur=(int)random(3);
switch(kleur){
case 0:stroke(255,255,255);break;
case 1:stroke(0,0,0);break;
case 2:
stroke(150,90,32);break;}
float y=random(height);
line(5*h,y,2+5*h,y+10);
}}